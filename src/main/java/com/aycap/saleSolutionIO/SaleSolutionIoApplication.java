package com.aycap.saleSolutionIO;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import com.aycap.saleSolutionIO.service.ExportExcelService;
import com.aycap.saleSolutionIO.service.ImportExcelService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@SpringBootApplication
public class SaleSolutionIoApplication implements ApplicationRunner {
//public class SaleSolutionIoApplication {
	
	@Value("${path.ACP:/D:/}")
	private String ACPpath;
	@Value("${path.KCC:/D:/}")
	private String KCCpath;

	public static void main(String[] args) {
		SpringApplication.run(SaleSolutionIoApplication.class, args);
	}
	@Autowired
	private ImportExcelService importService;
	@Autowired
	private ExportExcelService exportService;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		/**
		 * "manager" must come before "sale"
		 */

//		exportService.exportReport("20210302", "20210309", "06:00");

		importService.readExcel("branch", KCCpath);
		exportService.writeExcel("branch", KCCpath);
	}
	
//	/**
//	 * CMD
//	 */
//	@Bean
//	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
//		return args -> {
//			String cmd = "";
//			if (args != null && args.length > 0) {
//				cmd = args[0];
//			}
//
//			if ("import".equals(cmd)) {
//				log.info("Start import -> Branch");
//				importService.readExcel("branch", KCCpath);
//				exportService.writeExcel("branch", KCCpath);
//				
//				log.info("Start import -> Manager");
//				importService.readExcel("manager", KCCpath);
//				exportService.writeExcel("manager", KCCpath);
//				
//				log.info("Start import -> Sale");
//				importService.readExcel("sale", KCCpath);
//				exportService.writeExcel("sale", KCCpath);
//
//			} else if ("export".equals(cmd)) {
//				if (args != null && args.length == 5) {
//					log.info("Start export ...");
//					
//					if (args[4].equalsIgnoreCase("acp")) {
//						exportService.exportReport(args[1], args[2], args[3], args[4], ACPpath);
//						
//					} else if (args[4].equalsIgnoreCase("kcc")) {
//						exportService.exportReport(args[1], args[2], args[3], args[4], KCCpath);
//					}
//				}
//
//			} 
//		};
//	}

}
