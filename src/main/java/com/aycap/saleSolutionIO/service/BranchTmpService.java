package com.aycap.saleSolutionIO.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.aycap.saleSolutionIO.helper.CalendarHelper;
import com.aycap.saleSolutionIO.model.Branch;
import com.aycap.saleSolutionIO.model.BranchTmp;
import com.aycap.saleSolutionIO.repository.BranchRepository;
import com.aycap.saleSolutionIO.repository.BranchTmpRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BranchTmpService extends BaseService<BranchTmp, Integer, BranchTmpRepository> {
	@Autowired
	BranchRepository branchRepository;
	@Autowired
	CalendarHelper calendarHelper;
	@Autowired
	ExportExcelService exportExcelService;
//	@Value("${path.outputLocation:/D:/}")
//	private String outputLocation;
	
	public List<BranchTmp> saveToDb(Map<Integer, List<String>> workbook) {
		List<BranchTmp> branchTmpList = new ArrayList<BranchTmp>();
		List<Branch> branchList = new ArrayList<Branch>();
		
		Integer numRecord = workbook.size();
		int i = 1;
		while (i < numRecord) {
			List<String> tmp = workbook.get(i);
			/*
			 * build BRANCH_TMP
			 */
			BranchTmp branchTmp = BranchTmp.builder()
					.bankId(tmp.get(0))
					.entity(tmp.get(1))
					.name(tmp.get(2))
					.latitude(tmp.get(3))
					.longitude(tmp.get(4))
					.action(tmp.get(5))
					.result(this.isValidField(tmp))
					.timestamp(new Date())
					.build();
			branchTmpList.add(branchTmp);
			/*
			 * build BRANCH
			 */
			if (this.isValidField(tmp).equalsIgnoreCase("pass")) {
				
				if (tmp.get(5).equalsIgnoreCase("d")) {
					branchRepository.deleteByBankId(tmp.get(0));
					
				} else {
					Integer branchId = this.getBranchIdByBankId(tmp.get(0));
					if (branchId != null) {
						Branch branch = Branch.builder()
								.id(branchId)
								.bankId(tmp.get(0))
								.entity(tmp.get(1))
								.name(tmp.get(2))
								.latitude(Double.parseDouble(tmp.get(3)))
								.longitude(Double.parseDouble(tmp.get(4)))
								.build();
						branchList.add(branch);
					} else {
						Branch branch = Branch.builder()
								.bankId(tmp.get(0))
								.entity(tmp.get(1))
								.name(tmp.get(2))
								.latitude(Double.parseDouble(tmp.get(3)))
								.longitude(Double.parseDouble(tmp.get(4)))
								.build();
						branchList.add(branch);
					}
					log.info(branchList.toString());
				}
			}
			i++;
		}
		/*
		 * save BRANCH
		 */
		branchRepository.saveAll(branchList);
		/*
		 * save BRANCH_TMP
		 */
		repository.saveAll(branchTmpList);
		
		return branchTmpList;
	}
	public Integer getBranchIdByBankId(String bankId) {
		Branch branch = branchRepository.findFirstByBankId(bankId);
		try {
			if (branch != null) {
				return branch.getId();
			}
			return null;
		} catch(NullPointerException e) {
			return null;
		}
	}
	/**
	 * @desc to check valid input
	 */
	public String isValidField(List<String> input) {
		String result = "";
		/*
		 * Branch ID
		 */
		if (this.isNullOrEmpty(input.get(0))) {
			result = result + "Branch ID is blank.";
		}
		/*
		 * Entity must be 3-digit.
		 */
		if (this.isNullOrEmpty(input.get(1))) {
			result = result + "Entity is blank.";
		} else if (input.get(1).length() != 3) {
			result = result + "Entity is not a 3-digit.";
		}
		/*
		 * Branch name
		 */
		if (this.isNullOrEmpty(input.get(2))) {
			result = result + "Branch name is blank.";
		}
		/*
		 * Longitude
		 */
		if (this.isNullOrEmpty(input.get(4))) {
			result = result + "Longitude is blank.";
		} else if (!this.isNumeric(input.get(4))) {
			result = result + "Longitude is non-numeric.";
		}
		/*
		 * Latitude
		 */
		if (this.isNullOrEmpty(input.get(3))) {
			result = result + "Latitude is blank.";
		} else if (!this.isNumeric(input.get(3))) {
			result = result + "Latitude is non-numeric.";
		}
		/*
		 * Action
		 */
		if (!input.get(5).equalsIgnoreCase("d") && !this.isNullOrEmpty(input.get(5))) {
			result = result + "Action is invalid.";
		}
		/*
		 * check for Null result
		 */
		if (this.isNullOrEmpty(result)) {
			result = result + "Pass";
		}
		return result;
	}
	/**
	 * @desc to check Numeric value
	 */
	public Boolean isNumeric(String input) {
		Boolean numeric = true;
		try {
			Double num = Double.parseDouble(input);
		} catch (NumberFormatException e) {
			numeric = false;
		}
		return numeric;
	}
	/**
	 * @desc to check Null or Empty value
	 */
	public Boolean isNullOrEmpty(String input) {
		
		Boolean isNull = false;
		try {
			if (input == null || input.trim().isEmpty()) {
				isNull = true;
			}
		} catch (NullPointerException e) {
			isNull = true;
		}
		return isNull;
	}
	/**
	 * @desc export BRANCH_TMP with Result
	 */
	public Workbook exportTmp(String outputLocation) {
		List<String> header = Arrays.asList("Branch ID", "Entity", "Branch Name", "Latitude", "Longitude", "Action", "Result", "Timestamp");

		Workbook headerWorkbook = exportExcelService.writeHeaderLine(header);
		Workbook finalWorkbook = this.writeDataLines(headerWorkbook);
		exportExcelService.saveWorkbook(outputLocation, "MasterData_Branch_" + calendarHelper.currentDateFormat(), finalWorkbook);
		return finalWorkbook;
	}
	public Workbook writeDataLines(Workbook workbook) {
		List<BranchTmp> branchTmpList = repository.findByTimestampBetweenOrderByTimestampDesc(calendarHelper.startDay(), calendarHelper.endDay());
		
		Sheet sheet = workbook.getSheet("Output");
        int rowCount = 1;
        
        for (BranchTmp tmp : branchTmpList) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;
             
            exportExcelService.createCell(row, columnCount++, tmp.getBankId());
            exportExcelService.createCell(row, columnCount++, tmp.getEntity());
            exportExcelService.createCell(row, columnCount++, tmp.getName());
            exportExcelService.createCell(row, columnCount++, tmp.getLatitude());
            exportExcelService.createCell(row, columnCount++, tmp.getLongitude());
            exportExcelService.createCell(row, columnCount++, tmp.getAction());
            exportExcelService.createCell(row, columnCount++, tmp.getResult());
            exportExcelService.createCell(row, columnCount++, tmp.getTimestamp().toString());
        }
        return workbook;
    }
}
