package com.aycap.saleSolutionIO.service;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.aycap.saleSolutionIO.helper.CalendarHelper;
import com.aycap.saleSolutionIO.model.Manager;
import com.aycap.saleSolutionIO.model.Tracking;
import com.aycap.saleSolutionIO.repository.ManagerRepository;
import com.aycap.saleSolutionIO.repository.TrackingRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TrackingService extends BaseService<Tracking, String, TrackingRepository> {
	@Autowired
	CalendarHelper calendarHelper;
	@Autowired
	ExportExcelService exportExcelService;
	@Autowired
	ManagerRepository managerRepository;
//	@Value("${path.reportLocation:/D:/}")
//	private String reportLocation;
//	private String currentDirReportLocation;
	
	private Date start;
	private Date end;
	
	public void exportTracking(Date dateGte, Date dateLte, String entityCode, String entityPath) {
		this.start = dateGte;
		this.end = dateLte;
		
	    String strDate = calendarHelper.currentDateFormat(); 
	    String dirReportLocation = entityPath + "TamNut\\Prod\\Report\\";
	    String fileName = entityCode + "_" + strDate;
	    
	    this.exportTrackingByEntity(entityCode, dirReportLocation, fileName);
	    
//	    File theDir = new File(currentDirReportLocation);
//		if (!theDir.exists()){
//		    theDir.mkdirs();
//		}
//		
//		List<Manager> managerList = managerRepository.findAll();
//		for(Manager manager : managerList) {
//			String managerId = manager.getEmployeeId();
//			this.exportTrackingByManagerId(managerId);
//		}
	}
	
	public Workbook exportTrackingByEntity(String entityCode, String dirReportLocation, String fileName) {
		List<String> header = Arrays.asList("Check Type", "Check-In/Check-Out", "Date Time", "Entity", "Employee ID", "Sale ID", "First Name", "Last Name", "Branch Name", "Distance from branch (km)", "Longitude", "Latitude", "Work Hours (min)", "AM Code", "Manager ID");

		Workbook headerWorkbook = exportExcelService.writeHeaderLine(header);
		Workbook finalWorkbook = this.writeDataLines(headerWorkbook, entityCode);
		exportExcelService.saveWorkbook(dirReportLocation, fileName, finalWorkbook);
		return finalWorkbook;
	}
	
	public Workbook writeDataLines(Workbook workbook, String entityCode) {
		List<Tracking> trackingList = repository.findBySale_EntityAndTimestampBetweenOrderByTimestampDesc(entityCode, this.start, this.end);
		
		Sheet sheet = workbook.getSheet("Output");
        int rowCount = 1;
        
        for (Tracking tracking : trackingList) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;
             
            exportExcelService.createCell(row, columnCount++, tracking.getCheckType().toString());
            exportExcelService.createCell(row, columnCount++, tracking.getType().toString());
            exportExcelService.createCell(row, columnCount++, tracking.getTimestamp().toString());
            exportExcelService.createCell(row, columnCount++, tracking.getSale().getEntity());
            exportExcelService.createCell(row, columnCount++, tracking.getSale().getEmployeeId());
            exportExcelService.createCell(row, columnCount++, tracking.getSale().getSaleId());
            exportExcelService.createCell(row, columnCount++, tracking.getSale().getFirstName());
            exportExcelService.createCell(row, columnCount++, tracking.getSale().getLastName());
            if(tracking.getBranch() != null) {
            	try {
            		exportExcelService.createCell(row, columnCount++, tracking.getBranch().getName());
            	} catch (NullPointerException e) {
            		exportExcelService.createCell(row, columnCount++, "");
            	}
            } else {
            	exportExcelService.createCell(row, columnCount++, "");
            }
            exportExcelService.createCell(row, columnCount++, tracking.getDistance());
            exportExcelService.createCell(row, columnCount++, tracking.getLongitude());
            exportExcelService.createCell(row, columnCount++, tracking.getLatitude());
            exportExcelService.createCell(row, columnCount++, tracking.getWorkHours());
            exportExcelService.createCell(row, columnCount++, managerRepository.findAmCodeByEmployeeId(tracking.getSale().getManagerId()));
            exportExcelService.createCell(row, columnCount++, tracking.getSale().getManagerId());
        }
        
        return workbook;
	}
	
//	public Workbook exportTrackingByManagerId(String managerId) {
//		List<String> header = Arrays.asList("Check Type", "Check-In/Check-Out", "Date Time", "Entity", "Employee ID", "Sale ID", "First Name", "Last Name", "Branch Name", "Distance from branch (km)", "Longitude", "Latitude", "Work Hours (min)", "AM Code");
//
//		Workbook headerWorkbook = exportExcelService.writeHeaderLine(header);
//		Workbook finalWorkbook = this.writeDataLines(headerWorkbook, managerId);
//		exportExcelService.saveWorkbook(currentDirReportLocation, managerId, finalWorkbook);
//		return finalWorkbook;
//	}
//	public Workbook writeDataLines(Workbook workbook, String managerId) {
//		List<Tracking> trackingList = repository.findBySale_ManagerIdAndTimestampBetweenOrderByTimestampDesc(managerId, this.start, this.end);
//		
//		Sheet sheet = workbook.getSheet("Output");
//        int rowCount = 1;
//        
//        for (Tracking tracking : trackingList) {
//            Row row = sheet.createRow(rowCount++);
//            int columnCount = 0;
//             
//            exportExcelService.createCell(row, columnCount++, tracking.getCheckType().toString());
//            exportExcelService.createCell(row, columnCount++, tracking.getType().toString());
//            exportExcelService.createCell(row, columnCount++, tracking.getTimestamp().toString());
//            exportExcelService.createCell(row, columnCount++, tracking.getSale().getEntity());
//            exportExcelService.createCell(row, columnCount++, tracking.getSale().getEmployeeId());
//            exportExcelService.createCell(row, columnCount++, tracking.getSale().getSaleId());
//            exportExcelService.createCell(row, columnCount++, tracking.getSale().getFirstName());
//            exportExcelService.createCell(row, columnCount++, tracking.getSale().getLastName());
//            if(tracking.getBranch() != null) {
//            	try {
//            		exportExcelService.createCell(row, columnCount++, tracking.getBranch().getName());
//            	} catch (NullPointerException e) {
//            		exportExcelService.createCell(row, columnCount++, "");
//            	}
//            } else {
//            	exportExcelService.createCell(row, columnCount++, "");
//            }
//            exportExcelService.createCell(row, columnCount++, tracking.getDistance());
//            exportExcelService.createCell(row, columnCount++, tracking.getLongitude());
//            exportExcelService.createCell(row, columnCount++, tracking.getLatitude());
//            exportExcelService.createCell(row, columnCount++, tracking.getWorkHours());
//            exportExcelService.createCell(row, columnCount++, managerRepository.findAmCodeByEmployeeId(tracking.getSale().getManagerId()));
//        }
//        
//        return workbook;
//	}
}
