package com.aycap.saleSolutionIO.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.aycap.saleSolutionIO.helper.CalendarHelper;
import com.aycap.saleSolutionIO.model.Sale;
import com.aycap.saleSolutionIO.model.SaleTmp;
import com.aycap.saleSolutionIO.repository.BranchRepository;
import com.aycap.saleSolutionIO.repository.ManagerRepository;
import com.aycap.saleSolutionIO.repository.SaleRepository;
import com.aycap.saleSolutionIO.repository.SaleTmpRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class SaleTmpService extends BaseService<SaleTmp, String, SaleTmpRepository> {
	@Autowired
	SaleRepository saleRepository;
	@Autowired
	ManagerRepository managerRepository;
	@Autowired
	BranchRepository branchRepository;
	@Autowired
	CalendarHelper calendarHelper;
	@Autowired
	ExportExcelService exportExcelService;
//	@Value("${path.outputLocation:/D:/}")
//	private String outputLocation;
	
	public List<SaleTmp> saveToDb(Map<Integer, List<String>> workbook) {
		List<SaleTmp> saleTmpList = new ArrayList<SaleTmp>();
		List<Sale> saleList = new ArrayList<Sale>();
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		Integer numRecord = workbook.size();
		
		int i = 1;
		while (i < numRecord) {
			List<String> tmp = workbook.get(i);
			/*
			 * build SALE_TMP
			 */
			SaleTmp saleTmp = SaleTmp.builder()
					.employeeId(tmp.get(0))
					.entity(tmp.get(1))
					.saleId(tmp.get(2))
					.firstName(tmp.get(3))
					.lastName(tmp.get(4))
					.bankId(tmp.get(5))
					.managerId(tmp.get(6))
					.action(tmp.get(7))
					.result(this.isValidField(tmp))
					.timestamp(new Date())
					.build();
			saleTmpList.add(saleTmp);
			/*
			 * build SALE
			 */
			if (this.isValidField(tmp).equalsIgnoreCase("pass")) {
				Sale sale = Sale.builder()
						.username(tmp.get(0))
						.password(bcrypt.encode(tmp.get(0)))
						.employeeId(tmp.get(0))
						.entity(tmp.get(1))
						.saleId(tmp.get(2))
						.firstName(tmp.get(3))
						.lastName(tmp.get(4))
						.bankId(tmp.get(5))
						.managerId(tmp.get(6))
						.build();
				if (tmp.get(7).equalsIgnoreCase("d")) {
//					saleRepository.deleteByEmployeeId(tmp.get(0));
					sale.setIsActive("F");
					
				} else {
					sale.setIsActive("T");
				}
				saleList.add(sale);
			}
			i++;
		}
		repository.saveAll(saleTmpList);
		saleRepository.saveAll(saleList);
		return saleTmpList;
	}
	/**
	 * @desc to check Valid input
	 */
	public String isValidField(List<String> input) {
		String result = "";
		/*
		 * Employee ID
		 */
		if (this.isNullOrEmpty(input.get(0))) {
			result = result + "Employee ID is blank.";
		} else if (input.get(0).length() < 8 || input.get(0).length() > 9) {
			result = result + "Employee ID is not a 8-digit or 9-digit.";
		}
		/*
		 * Entity must be 3-digit number.
		 */
		if (this.isNullOrEmpty(input.get(1))) {
			result.concat("Entity is blank.");
		} else if (input.get(1).length() != 3) {
			result = result + "Entity is not a 3-digit.";
		}
		/*
		 * Sale ID
		 */
		if (this.isNullOrEmpty(input.get(2))) {
			result.concat("Sale ID is blank.");
		}
		/*
		 * Firstname
		 */
		if (this.isNullOrEmpty(input.get(3))) {
			result = result + "Firstname is blank.";
		} else if (input.get(3).length() > 100) {
			result = result + "Firstname is over the size limit.";
		}
		/*
		 * Lastname
		 */
		if (this.isNullOrEmpty(input.get(4))) {
			result = result + "Lastname is blank.";
		} else if (input.get(4).length() > 100) {
			result = result + "Lastname is over the size limit.";
		}
		/*
		 * Bank ID
		 */
		if (this.isNullOrEmpty(input.get(5))) {
			result = result + "Branch ID is blank.";
		} else if (branchRepository.findByBankId(input.get(5)).size() < 1) {
			result = result + "Not found Branch ID.";
		}
		/*
		 * Manager ID
		 */
		if (this.isNullOrEmpty(input.get(6))) {
			result = result + "Manager ID is blank.";
		} else if (input.get(6).length() < 8 || input.get(6).length() > 9) {
			result = result + "Manager ID is not a 8-digit or 9-digit.";
		} else if (managerRepository.findByEmployeeId(input.get(6)).size() < 1) {
			result = result + "Not found Manager ID.";
		}
		/*
		 * Action
		 */
		if (!input.get(7).equalsIgnoreCase("d") && !this.isNullOrEmpty(input.get(7))) {
			result = result + "Action is invalid.";
		}
		/*
		 * check for Null result
		 */
		if (this.isNullOrEmpty(result)) {
			result = result + "Pass";
		}
		return result;
	}
	/**
	 * @desc to check Null or Empty value
	 */
	public Boolean isNullOrEmpty(String input) {
		Boolean isNull = false;
		try {
			if (input == null || input.trim().isEmpty()) {
				isNull = true;
			} else {
				isNull = false;
			}
		} catch (NullPointerException e) {
			isNull = false;
		}
		return isNull;
	}
	/**
	 * @desc export BRANCH_TMP with Result
	 */
	public Workbook exportTmp(String outputLocation) {
		List<String> header = Arrays.asList("Employee ID", "Entity", "Sale ID", "First Name", "Last Name", "Branch ID", "AM Employee ID", "Action", "Result", "Timestamp");

		Workbook headerWorkbook = exportExcelService.writeHeaderLine(header);
		Workbook finalWorkbook = this.writeDataLines(headerWorkbook);
		exportExcelService.saveWorkbook(outputLocation, "MasterData_PC_" + calendarHelper.currentDateFormat(), finalWorkbook);
		return finalWorkbook;
	}
	public Workbook writeDataLines(Workbook workbook) {
		List<SaleTmp> saleTmpList = repository.findByTimestampBetweenOrderByTimestampDesc(calendarHelper.startDay(), calendarHelper.endDay());
		
		Sheet sheet = workbook.getSheet("Output");
        int rowCount = 1;
        
        for (SaleTmp tmp : saleTmpList) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;
             
            exportExcelService.createCell(row, columnCount++, tmp.getEmployeeId());
            exportExcelService.createCell(row, columnCount++, tmp.getEntity());
            exportExcelService.createCell(row, columnCount++, tmp.getSaleId());
            exportExcelService.createCell(row, columnCount++, tmp.getFirstName());
            exportExcelService.createCell(row, columnCount++, tmp.getLastName());
            exportExcelService.createCell(row, columnCount++, tmp.getBankId());
            exportExcelService.createCell(row, columnCount++, tmp.getManagerId());
            exportExcelService.createCell(row, columnCount++, tmp.getAction());
            exportExcelService.createCell(row, columnCount++, tmp.getResult());
            exportExcelService.createCell(row, columnCount++, tmp.getTimestamp().toString());
        }
        return workbook;
    }
}
