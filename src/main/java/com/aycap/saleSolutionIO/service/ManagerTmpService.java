package com.aycap.saleSolutionIO.service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import com.aycap.saleSolutionIO.helper.CalendarHelper;
import com.aycap.saleSolutionIO.model.Manager;
import com.aycap.saleSolutionIO.model.ManagerTmp;
import com.aycap.saleSolutionIO.repository.ManagerRepository;
import com.aycap.saleSolutionIO.repository.ManagerTmpRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ManagerTmpService extends BaseService<ManagerTmp, String, ManagerTmpRepository> {
	@Autowired
	ManagerRepository managerRepository;
	@Autowired
	CalendarHelper calendarHelper;
	@Autowired
	ExportExcelService exportExcelService;
//	@Value("${path.outputLocation:/D:/}")
//	private String outputLocation;
	
	public List<ManagerTmp> saveToDb(Map<Integer, List<String>> workbook) {
		List<ManagerTmp> managerTmpList = new ArrayList<ManagerTmp>();
		List<Manager> managerList = new ArrayList<Manager>();
		BCryptPasswordEncoder bcrypt = new BCryptPasswordEncoder();
		Integer numRecord = workbook.size();

		int i = 1;
		while (i < numRecord) {
			List<String> tmp = workbook.get(i);
			/*
			 * build MANAGER_TMP
			 */
			ManagerTmp managerTmp = ManagerTmp.builder()
					.employeeId(tmp.get(0))
					.entity(tmp.get(1))
					.amCode(tmp.get(2))
					.firstName(tmp.get(3))
					.lastName(tmp.get(4))
					.action(tmp.get(5))
					.result(this.isValidField(tmp))
					.timestamp(new Date())
					.build();
			managerTmpList.add(managerTmp);
			/*
			 * build MANAGER
			 */
			if (this.isValidField(tmp).equalsIgnoreCase("pass")) {
				Manager manager = Manager.builder()
						.username(tmp.get(0))
						.password(bcrypt.encode(tmp.get(0)))
						.employeeId(tmp.get(0))
						.entity(tmp.get(1))
						.amCode(tmp.get(2))
						.firstName(tmp.get(3))
						.lastName(tmp.get(4))
						.build();
				if (tmp.get(5).equalsIgnoreCase("d")) {
//					managerRepository.deleteByEmployeeId(tmp.get(0));
					manager.setIsActive("F");
				} else {
					manager.setIsActive("T");
				}
				managerList.add(manager);
			}
			i++;
		}
		repository.saveAll(managerTmpList);
		managerRepository.saveAll(managerList);
		return managerTmpList;
	}
	/**
	 * @desc to check valid input
	 */
	public String isValidField(List<String> input) {
		String result = "";
		/*
		 * Employee ID
		 */
		if (this.isNullOrEmpty(input.get(0))) {
			result = result + "Employee ID is blank.";
		} else if (input.get(0).length() < 8 || input.get(0).length() > 9) {
			result = result + "Employee ID is not a 8-digit or 9-digit.";
		}
		/*
		 * Entity must be 3-digit number.
		 */
		if (this.isNullOrEmpty(input.get(1))) {
			result = result + "Entity is blank.";
		} else if (input.get(1).length() != 3) {
			result = result + "Entity is not a 3-digit.";
		}
		/*
		 * AM Code
		 */
		if (this.isNullOrEmpty(input.get(2))) {
			result = result + "AM Code is blank.";
		}
		/*
		 * Firstname
		 */
		if (this.isNullOrEmpty(input.get(3))) {
			result = result + "Firstname is blank.";
		} else if (input.get(3).length() > 100) {
			result = result + "Firstname is over the size limit.";
		}
		/*
		 * Lastname
		 */
		if (this.isNullOrEmpty(input.get(4))) {
			result = result + "Lastname is blank.";
		} else if (input.get(4).length() > 100) {
			result = result + "Lastname is over the size limit.";
		}
		/*
		 * Action
		 */
		if (!input.get(5).equalsIgnoreCase("d") && !this.isNullOrEmpty(input.get(5))) {
			result = result + "Action is invalid.";
		}
		/*
		 * check for Null result
		 */
		if (this.isNullOrEmpty(result)) {
			result = result + "Pass";
		}
		return result;
	}
	/**
	 * @desc to check Null or Empty value
	 */
	public Boolean isNullOrEmpty(String input) {
		Boolean isNull = false;
		try {
			if (input == null || input.trim().isEmpty()) {
				isNull = true;
			} else {
				isNull = false;
			}
		} catch (NullPointerException e) {
			isNull = false;
		}
		return isNull;
	}
	/**
	 * @desc export BRANCH_TMP with Result
	 */
	public Workbook exportTmp(String outputLocation) {
		List<String> header = Arrays.asList("Employee ID", "Entity", "AM Code", "First Name", "Last Name", "Action", "Result", "Timestamp");

		Workbook headerWorkbook = exportExcelService.writeHeaderLine(header);
		Workbook finalWorkbook = this.writeDataLines(headerWorkbook);
		exportExcelService.saveWorkbook(outputLocation, "MasterData_AM_" + calendarHelper.currentDateFormat(), finalWorkbook);
		return finalWorkbook;
	}
	public Workbook writeDataLines(Workbook workbook) {
		List<ManagerTmp> managerTmpList = repository.findByTimestampBetweenOrderByTimestampDesc(calendarHelper.startDay(), calendarHelper.endDay());
		
		Sheet sheet = workbook.getSheet("Output");
        int rowCount = 1;
        
        for (ManagerTmp tmp : managerTmpList) {
            Row row = sheet.createRow(rowCount++);
            int columnCount = 0;
             
            exportExcelService.createCell(row, columnCount++, tmp.getEmployeeId());
            exportExcelService.createCell(row, columnCount++, tmp.getEntity());
            exportExcelService.createCell(row, columnCount++, tmp.getAmCode());
            exportExcelService.createCell(row, columnCount++, tmp.getFirstName());
            exportExcelService.createCell(row, columnCount++, tmp.getLastName());
            exportExcelService.createCell(row, columnCount++, tmp.getAction());
            exportExcelService.createCell(row, columnCount++, tmp.getResult());
            exportExcelService.createCell(row, columnCount++, tmp.getTimestamp().toString());
        }
        return workbook;
    }
}
