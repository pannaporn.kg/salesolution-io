package com.aycap.saleSolutionIO.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.FillPatternType;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import com.aycap.saleSolutionIO.model.BranchTmp;
import com.aycap.saleSolutionIO.model.ManagerTmp;
import com.aycap.saleSolutionIO.repository.ManagerTmpRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ImportExcelService extends BaseService<ManagerTmp, String, ManagerTmpRepository> {
	
//	@Value("${path.managerLocation:/D:/}")
//	private String managerLocation;
//	@Value("${path.saleLocation:/D:/}")
//	private String saleLocation;
//	@Value("${path.branchLocation:/D:/}")
//	private String branchLocation;
//	@Value("${path.fileLocation:/D:/}")
//	private String fileLocation;
	
	@Autowired
	private ManagerTmpService managerTmpService;
	@Autowired
	private SaleTmpService saleTmpService;
	@Autowired
	private BranchTmpService branchTmpService;
	
	/**
	 * 
	 * @desc Generate "file path" from "Entity"
	 */
	public String getFileLocation(String type, String entityPath) {
		if (type.equalsIgnoreCase("manager")) {
			return entityPath + "TamNut\\Prod\\Input\\MasterData_AM.xlsx";
		} else if (type.equalsIgnoreCase("sale")) {
			return entityPath + "TamNut\\Prod\\Input\\MasterData_PC.xlsx";
		} else if (type.equalsIgnoreCase("branch")) {
			return entityPath + "TamNut\\Prod\\Input\\MasterData_Branch.xlsx";
		} else {
			return null;
		}
	}
	/**
	 * 
	 * @desc Get "file input stream" from "file path"
	 */
	public FileInputStream getFileStream(String fileLocation) {
		try {
			FileInputStream fileStream = new FileInputStream(new File(fileLocation));
			return fileStream;
		} catch(NullPointerException e) {
			return null;
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		return null;
	}
		
//	public FileInputStream getFile(String type) {
//		FileInputStream file;
//		try {
//			if (type.equalsIgnoreCase("manager")) {
//				file = new FileInputStream(new File(managerLocation));
//			} else if (type.equalsIgnoreCase("sale")) {
//				file = new FileInputStream(new File(saleLocation));
//			} else if (type.equalsIgnoreCase("branch")) {
//				file = new FileInputStream(new File(branchLocation));
//			} else {
//				return null;
//			}
//			return file;
//		} catch(NullPointerException e) {
//			return null;
//		} catch (FileNotFoundException e) {
//			e.printStackTrace();
//		}
//		return null;
//	}
	/**
	 * 
	 * @desc Delete file after importing
	 */
	public void deleteFile(String fileLocation) {
		File file = new File(fileLocation);
		if (file.delete()) {
			log.info("DELETED");
		}
	}
	/**
	 * 
	 * @desc Reading/Importing from Excel
	 */
	public Workbook readExcel(String type, String entityPath) {

		try {
//			Workbook workbook = new XSSFWorkbook(this.getFile(type));
			Workbook workbook = new XSSFWorkbook(this.getFileStream(this.getFileLocation(type, entityPath)));
			
			Sheet sheet = workbook.getSheetAt(0);
			Map<Integer, List<String>> data = new HashMap<>();
			int i = 0;
			for (Row row : sheet) {
				data.put(i, new ArrayList<String>());
				for (Cell cell : row) {
					cell.setCellType(CellType.STRING);
					
					switch (cell.getCellType()) {
			            case STRING:
			            	data.get(new Integer(i)).add(cell.getRichStringCellValue().getString());
			            	break;
			            case NUMERIC:
			            	if (DateUtil.isCellDateFormatted(cell)) {
			            	    data.get(i).add(cell.getDateCellValue() + "");
			            	} else {
			            	    data.get(i).add(cell.getNumericCellValue() + "");
			            	}
			            	break;
			            case BOOLEAN:
			            	data.get(i).add(cell.getBooleanCellValue() + "");
			            	break;
			            case FORMULA:
			            	data.get(i).add(cell.getCellFormula() + "");
			            	break;
			            default: data.get(new Integer(i)).add(" ");
					}
				}
				i++;
			}
			workbook.close();
			
			this.saveToDb(type, data, entityPath);
			return workbook;
			
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch(NullPointerException e) {
			return null;
		}
		return null;
	}
	/**
	 * 
	 * @desc Save data to DB after reading
	 */
	public void saveToDb(String type, Map<Integer, List<String>> workbook, String entityPath) {
		if (type.equalsIgnoreCase("manager")) {
			managerTmpService.saveToDb(workbook);
//			this.deleteFile(managerLocation);

		} else if (type.equalsIgnoreCase("sale")) {
			saleTmpService.saveToDb(workbook);
//			this.deleteFile(saleLocation);
			
		} else if (type.equalsIgnoreCase("branch")) {
			branchTmpService.saveToDb(workbook);
//			this.deleteFile(branchLocation);
		}
		
		this.deleteFile(this.getFileLocation(type, entityPath));
	}
	
}
