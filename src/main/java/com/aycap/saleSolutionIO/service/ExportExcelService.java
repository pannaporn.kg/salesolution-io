package com.aycap.saleSolutionIO.service;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFFont;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ExportExcelService {
	@Autowired
	ManagerTmpService managerTmpService;
	@Autowired
	SaleTmpService saleTmpService;
	@Autowired
	BranchTmpService branchTmpService;
	@Autowired
	TrackingService trackingService;
	
	/**
	 * 
	 * @desc Generate Output Excel
	 */
	public void writeExcel(String type, String entityPath) {
		if (type.equalsIgnoreCase("manager")) {
			managerTmpService.exportTmp(entityPath + "TamNut\\Prod\\Output\\");
		} else if (type.equalsIgnoreCase("sale")) {
			saleTmpService.exportTmp(entityPath + "TamNut\\Prod\\Output\\");
		} else if (type.equalsIgnoreCase("branch")) {
			branchTmpService.exportTmp(entityPath + "TamNut\\Prod\\Output\\");
		}
	}
	/**
	 * 
	 * @desc Generate Report
	 */
	public void exportReport(String startDate, String endDate, String time, String entityCode, String entityPath) throws ParseException {
		SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd'T'HH:mm",Locale.ENGLISH);
		Date dateGte = format.parse(startDate + "T" + time);
		Date dateLte = format.parse(endDate + "T" + time);
		log.info(dateGte.toString());
		log.info(dateLte.toString());
		trackingService.exportTracking(dateGte, dateLte, entityCode, entityPath);
	}

    public Workbook writeHeaderLine(List<String> header) {
    	Workbook workbook = new XSSFWorkbook();

		Sheet sheet = workbook.createSheet("Output");
		Row row = sheet.createRow(0);
		CellStyle headerStyle = workbook.createCellStyle();
		 
		XSSFFont font = ((XSSFWorkbook) workbook).createFont();
		font.setBold(true);
		headerStyle.setFont(font);
		
		int i = 0;
		for (String word : header) {
			createCell(row, i, word);
			i++;
		}
        return workbook;
         
    }
	public void createCell(Row row, int columnCount, Object value) {
        Cell cell = row.createCell(columnCount);
        if (value instanceof Integer) {
            cell.setCellValue((Integer) value);
        } else if (value instanceof Boolean) {
            cell.setCellValue((Boolean) value);
        } else if (value instanceof Double) {
            cell.setCellValue((Double) value);
        } else {
            cell.setCellValue((String) value);
        }
    }
	public void saveWorkbook(String fileLocation, String fileName, Workbook workbook) {
		try {
			FileOutputStream outputStream = new FileOutputStream(fileLocation + fileName + ".xlsx");
			workbook.write(outputStream);
			workbook.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
