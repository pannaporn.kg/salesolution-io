package com.aycap.saleSolutionIO.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.aycap.saleSolutionIO.model.Manager;

@Repository
public interface ManagerRepository extends JpaRepository<Manager, String> {
	public String deleteByEmployeeId(String employeeId);
	public Manager findFirstByEmployeeId(String employeeId);
	
	@Query(value = "SELECT AM_CODE FROM MANAGER WHERE MANAGER.EMPLOYEE_ID = ?1", nativeQuery = true)
	public String findAmCodeByEmployeeId(String employeeId);
	
	public List<Manager> findByEmployeeId(String EmployeeId);
}
