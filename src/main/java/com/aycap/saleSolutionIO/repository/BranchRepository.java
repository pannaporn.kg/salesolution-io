package com.aycap.saleSolutionIO.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aycap.saleSolutionIO.model.Branch;

@Repository
public interface BranchRepository extends JpaRepository<Branch, Integer> {
	public String deleteByBankId(String bankId);
	public Branch findFirstByBankId(String bankId);
	public List<Branch> findByBankId(String bankId);
}
