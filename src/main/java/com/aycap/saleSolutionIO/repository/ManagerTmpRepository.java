package com.aycap.saleSolutionIO.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aycap.saleSolutionIO.model.ManagerTmp;
import com.aycap.saleSolutionIO.model.SaleTmp;

@Repository
public interface ManagerTmpRepository extends JpaRepository<ManagerTmp, String> {
	public List<ManagerTmp> findByTimestampBetweenOrderByTimestampDesc(Date dateGte, Date dateLte);
}
