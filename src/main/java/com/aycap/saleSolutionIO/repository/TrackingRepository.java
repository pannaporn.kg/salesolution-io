package com.aycap.saleSolutionIO.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aycap.saleSolutionIO.model.Tracking;

@Repository
public interface TrackingRepository extends JpaRepository<Tracking, String> {
	public List<Tracking> findByTimestampBetweenOrderByTimestampDesc(Date dateGte, Date dateLte);
	public List<Tracking> findBySale_ManagerIdAndTimestampBetweenOrderByTimestampDesc(String managerId, Date dateGte, Date dateLte);
	
	public List<Tracking> findBySale_EntityAndTimestampBetweenOrderByTimestampDesc(String entity, Date dateGte, Date dateLte);
}
