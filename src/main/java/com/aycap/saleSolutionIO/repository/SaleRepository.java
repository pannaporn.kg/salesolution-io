package com.aycap.saleSolutionIO.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aycap.saleSolutionIO.model.Sale;

@Repository
public interface SaleRepository extends JpaRepository<Sale, String> {
	public String deleteByEmployeeId(String employeeId);
	public List<Sale> findByEmployeeId(String EmployeeId);
}
