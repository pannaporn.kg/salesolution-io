package com.aycap.saleSolutionIO.repository;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.aycap.saleSolutionIO.model.BranchTmp;

@Repository
public interface BranchTmpRepository extends JpaRepository<BranchTmp, Integer> {
	public List<BranchTmp> findByTimestampBetweenOrderByTimestampDesc(Date dateGte, Date dateLte);
}
