package com.aycap.saleSolutionIO.model;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Transient;

import org.springframework.lang.Nullable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Tracking {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="saleUsername",referencedColumnName="username")
	private Sale sale;
	
	@ManyToOne(cascade=CascadeType.ALL)
	// Default referencedColumnName="id"
	@JoinColumn(name="branchId")
	private Branch branch;
	
	private TRACKING_TYPE type;
	private double longitude;
	private double latitude;
	private Date timestamp;
	private CHECK_TYPE checkType;
	@Nullable
	private double distance;
	@Nullable
	private double workHours;
	
	
	// Input only String
	@Transient
	private String bank_id;
}
