package com.aycap.saleSolutionIO.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Manager {
	@Id
	private String username;
	private String password;
	private String ref;
	private Date refTimestamp;
	private String isActive;
	
	private String firstName;
	private String lastName;
	private String employeeId;
	private String entity;
	
	private String amCode;
}
