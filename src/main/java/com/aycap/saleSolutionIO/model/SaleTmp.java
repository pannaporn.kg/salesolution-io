package com.aycap.saleSolutionIO.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SaleTmp {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	
	private String employeeId;
	private String firstName;
	private String lastName;
	private String managerId;	
	private String entity;
	private String bankId;
	private String saleId;
	
	private String action;
	private String result;
	private Date timestamp;
}
