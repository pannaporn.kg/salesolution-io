package com.aycap.saleSolutionIO.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Branch {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;

	private String bankId;
	private String entity;
	private String name;
	private double longitude;
	private double latitude;
}
