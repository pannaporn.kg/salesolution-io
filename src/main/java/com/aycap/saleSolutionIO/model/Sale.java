package com.aycap.saleSolutionIO.model;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Builder(toBuilder = true)
@NoArgsConstructor
@AllArgsConstructor
public class Sale {
	@Id
	private String username;
	private String password;
	private String ref;
	private Date refTimestamp;
	private String isActive;
	
	private String firstName;
	private String lastName;	
	private String employeeId;
	
//	@ManyToOne(cascade=CascadeType.ALL)
//	// Default referencedColumnName="id"
//	@JoinColumn(name="managerId")
//	private Manager manager;
	private String managerId;
	private String entity;
	private String bankId;
	
	private String saleId;
}
