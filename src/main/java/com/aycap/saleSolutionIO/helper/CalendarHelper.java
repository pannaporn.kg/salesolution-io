package com.aycap.saleSolutionIO.helper;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Service;

import com.aycap.saleSolutionIO.service.SaleTmpService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CalendarHelper {

	public Date startMonth() {
		Calendar start = this.setCalendar(0, 0, 0, 0);
		
		start.add(Calendar.MONTH, -1);
		start.set(Calendar.DAY_OF_MONTH, 1);
		
		log.info(start.getTime().toString());
		return start.getTime();
	}
	public Date endMonth() {
		Calendar end = this.setCalendar(0, 0, 0, 0);
		
		end.set(Calendar.DAY_OF_MONTH, 1);  
		
		log.info(end.getTime().toString());
		return end.getTime();
	}
	
	public Date startDay() {
		Calendar start = this.setCalendar(0, 0, 0, 0);

		log.info(start.getTime().toString());
		return start.getTime();
	}
	public Date endDay() {
		Calendar end = this.setCalendar(0, 0, 0, 0);
		end.add(Calendar.DATE, 1);

		log.info(end.getTime().toString());
		return end.getTime();
	}
	
	public Date startSixOclock() {
		Calendar start = this.setCalendar(6, 0, 0, 0);
		start.add(Calendar.DATE, -1);
		
		log.info(start.getTime().toString());
		return start.getTime();
	}
	public Date endSixOclock() {
		Calendar end = this.setCalendar(6, 0, 0, 0);
		
		log.info(end.getTime().toString());
		return end.getTime();
	}
	/**
	 * @desc Set calendar by hour, min, sec, millisec
	 */
	public Calendar setCalendar(Integer hour, Integer min, Integer sec, Integer millisec) {
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.HOUR_OF_DAY, hour);
		cal.set(Calendar.MINUTE, min);
		cal.set(Calendar.SECOND, sec);
		cal.set(Calendar.MILLISECOND, millisec);
		return cal;
	}
	
	public String currentDateFormat() {
		Date date = new Date();
		SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMdd_hhmmss");  
	    String strDate = formatter.format(date);  
	    return strDate;
	}
}
